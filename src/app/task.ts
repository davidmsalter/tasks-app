export interface Task {
  taskNo: number;
  title: string;
  description: string;
  priority: 'low' | 'medium' | 'high';
  due?: Date;
  completed?: Date;
}
