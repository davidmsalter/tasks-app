import { Component, OnInit } from '@angular/core';
import { TasksService } from '../tasks.service';
import { Task } from '../task';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css'],
})
export class TaskListComponent implements OnInit {
  constructor(private taskService: TasksService) {}

  tasks: Task[] = [];
  selectedTask?: Task;
  showAddTask = false;

  ngOnInit(): void {
    this.getTasks();
  }

  private getTasks() {
    this.tasks = this.taskService.getOpenTasks();
  }

  onConfirm(create: boolean) {
    console.log('onConfirm');
    this.showAddTask = false;
    this.getTasks();
  }
}
