import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TasksService } from '../tasks.service';

@Component({
  selector: 'app-task-reporter',
  templateUrl: './task-reporter.component.html',
  styleUrls: ['./task-reporter.component.css'],
})
export class TaskReporterComponent implements OnInit {
  @Output() confirm = new EventEmitter();
  @Input() showAddTask: boolean = false;

  taskForm: FormGroup | undefined;

  constructor(
    private formBuilder: FormBuilder,
    private taskService: TasksService
  ) {}

  ngOnInit(): void {
    this.taskForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
      priority: ['Medium', Validators.required],
      due: [''],
    });
  }

  addTask() {
    if (this.taskForm && this.taskForm.invalid) {
      this.taskForm.markAllAsTouched();
      return;
    }

    this.taskService.createTask(this.taskForm?.value);
    this.showAddTask = false;

    this.confirm.emit(true);
  }
}
