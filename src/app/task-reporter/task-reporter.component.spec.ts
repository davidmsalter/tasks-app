import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskReporterComponent } from './task-reporter.component';

describe('TaskReporterComponent', () => {
  let component: TaskReporterComponent;
  let fixture: ComponentFixture<TaskReporterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskReporterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskReporterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
