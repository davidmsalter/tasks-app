import { Injectable } from '@angular/core';
import { Task } from './task';

@Injectable({
  providedIn: 'root',
})
export class TasksService {
  private tasks: Task[] = [];

  constructor() {}

  getOpenTasks(): Task[] {
    return this.tasks.filter((task) => !task.completed);
  }

  createTask(task: Task) {
    task.taskNo = this.tasks.length + 1;
    this.tasks.push(task);
  }
}
